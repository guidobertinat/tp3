# tp3

Trabajo practico con dockerhub

#### Tarea Actualizacion Tecnologica 3 Kubernetes:
-  Subo una imagen creada por mi a docker-hub (guidob100/tp3) con una webapp en springboot para deployar y mostrar el adivinador de resultados segun equipo.
-  Ejecuto archivo de deployment para crear los pods con replicas.
-  Ejecuto servicio con yaml para exponer los puertos al exterior.
-  El servicio aplica un LoadBalancer, repartira la carga entre las consultas.


###### Pasos a seguir:

1. microk8s kubectl apply -f micro-dp.yaml  (aplico deployment)
2. microk8s kubectl get all (muestra todos los recursos de k8s)
3. microk8s kubectl apply -f micro-service.yaml (aplico servicio)
4. microk8s kubectl describe svc micro  (puedo ver los endpoints de cada uno de los pods, y tambien puedo ver el IP asignado para el servicio a nivel local y acceder)
5. Abrir navegador desde fuera de la virtual y con ip de la virtual mas el puerto asignado en el srv (+30000) acceder; luego podemos eliminar uno de los pods y demostramos que balancea los request ya que sigue funcionando. (Utilizar HTTP)

Ej. microk8s kubectl delete pod/resultado-7779fbdc68-v4zfc

Ej. http://192.168.0.198:31763/
  
Ej. http://192.168.0.198:31763/resultado
  
Ej. http://192.168.0.198:31763/resultado?local=Argentina&visitante=Chile




