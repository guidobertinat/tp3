FROM openjdk:11-jre-slim-buster
COPY tarea-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
RUN sh -c 'touch tarea-0.0.1-SNAPSHOT.jar'
ENTRYPOINT ["java","-jar","tarea-0.0.1-SNAPSHOT.jar"]
